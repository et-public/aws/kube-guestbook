# Guestbook service on Kubernetes cluster

## Prerequisites

- Tools: **awscli, aws-iam-authenticator, envsubst, go, jq, kubectl, kubectx, kubens, vault**
- Permission to use a Kubernetes cluster (config KUBECONFIG environment accordingly)
- Clone `kube-guestbook` repository

```console
$ git clone git@code.stanford.edu:et-public/kube-guestbook.git
```

## Configure Kubernetes authentication in a specific environment

There are *dev*, *stage*, and *prod* environments defined. To switch to a specific Kubernetes cluster to provision the guestbook, for a fully deployed app in dev, you will see the following Kubernetes resources:

```console
$ cd dev
$ source env.sh
$ $ kubectl get all,pv,pvc,secret  -n guestbook
NAME                               READY     STATUS    RESTARTS   AGE
pod/guestbook-8f8667d9c-dgsb8      1/1       Running   0          23h
pod/guestbook-8f8667d9c-dp5jl      1/1       Running   0          23h
pod/guestbook-8f8667d9c-nk8ll      1/1       Running   0          23h
pod/redis-master-bb4946d7b-2m9wn   1/1       Running   0          20h
pod/redis-slave-54574b8b98-h2v2c   1/1       Running   0          23h
pod/redis-slave-54574b8b98-jdsd7   1/1       Running   0          23h

NAME                   TYPE           CLUSTER-IP       EXTERNAL-IP                                                              PORT(S)         AGE
service/guestbook      LoadBalancer   172.20.232.207   aa4db85f680e211e8858f0a423f7fe82-609042958.us-west-2.elb.amazonaws.com   443:32263/TCP   6d
service/redis-master   ClusterIP      172.20.194.239   <none>                                                                   6379/TCP        6d
service/redis-slave    ClusterIP      172.20.74.150    <none>                                                                   6379/TCP        6d

NAME                           DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/guestbook      3         3         3            3           6d
deployment.apps/redis-master   1         1         1            1           6d
deployment.apps/redis-slave    2         2         2            2           6d

NAME                                      DESIRED   CURRENT   READY     AGE
replicaset.apps/guestbook-5c548f4769      0         0         0         6d
replicaset.apps/guestbook-64f9769d9d      0         0         0         1d
replicaset.apps/guestbook-8f8667d9c       3         3         3         23h
replicaset.apps/redis-master-55db5f7567   0         0         0         6d
replicaset.apps/redis-master-6bb77998db   0         0         0         20h
replicaset.apps/redis-master-6c46f6cfb8   0         0         0         22h
replicaset.apps/redis-master-bb4946d7b    1         1         1         20h
replicaset.apps/redis-slave-54574b8b98    2         2         2         23h
replicaset.apps/redis-slave-584c66c5b5    0         0         0         6d

NAME                                                        CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS    CLAIM                      STORAGECLASS     REASON    AGE
persistentvolume/pvc-8f064ddb-8555-11e8-858f-0a423f7fe828   100Gi      RWO            Delete           Bound     guestbook/guestbook-data   us-west-2a-gp2             20h

NAME                                   STATUS    VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS     AGE
persistentvolumeclaim/guestbook-data   Bound     pvc-8f064ddb-8555-11e8-858f-0a423f7fe828   100Gi      RWO            us-west-2a-gp2   20h

NAME                         TYPE                                  DATA      AGE
secret/default-token-bsbss   kubernetes.io/service-account-token   3         6d
secret/guestbook             kubernetes.io/tls                     2         4d
...
```

## Deployment in a specific environment

```
$ cd dev
$ edit env.sh
$ make provision
...
$ kubectl get pods -n guestbook
NAME                            READY     STATUS    RESTARTS   AGE
guestbook-67f65745c-65lg2       1/1       Running   0          11h
guestbook-67f65745c-cdr4n       1/1       Running   0          11h
guestbook-67f65745c-kc4rb       1/1       Running   0          11h
redis-master-585798d8ff-tfs8r   1/1       Running   0          11h
redis-slave-865486c9df-nbrrc    1/1       Running   0          11h
redis-slave-865486c9df-s6h7s    1/1       Running   0          11h
```

## Updating

If configurations, secretes, image release changes, just run `make update` to for a rolling upgrade.

## To Teardown

```console
$ cd dev
$ make DESTROY
```
__NOTE:__ `make DESTROY` will not destroy the persistent disk and service object.

To manually destroy disks:

```console
$ make destroy-disks
```

Destroying services will destroy ELB and associated security groups. Only do
this if you teardown the service. For regular deployment update, use `make update` instead.

```console
$ make destroy-svc
```
