THIS_MAKEFILE := $(realpath $(lastword $(MAKEFILE_LIST)))
SHELL := /bin/bash
PATH := ${SCRIPTS_DIR}:${PATH}
COMMON := ../common
TEMPLATES := ${COMMON}/templates
AWS_PROFILE := NODEFAULT
TIMESTAMP := $(shell date -u +"%Y-%m-%dT%H%M%SZ")
export

# Local includes
include ${COMMON}/env.sh
include env.sh

export
ifneq ($(wildcard ${SCRIPTS_DIR}/.*),)
        _ := $(shell cd ${SCRIPTS_DIR}; git pull)
else
        _ := $(shell git clone ${SCRIPTS_REPO} ${SCRIPTS_DIR})
endif

# COMMON MAKEFILE PARTS INCLUDES
include ${SCRIPTS_DIR}/makefile_parts/shared.mk
include ${SCRIPTS_DIR}/makefile_parts/config.mk
include ${SCRIPTS_DIR}/makefile_parts/vault.mk
include ${SCRIPTS_DIR}/makefile_parts/kube-sec.mk
include ${SCRIPTS_DIR}/makefile_parts/kube-cert.mk
include ${SCRIPTS_DIR}/makefile_parts/cert-manager-helm.mk
include ${SCRIPTS_DIR}/makefile_parts/disk.mk
# END COMMON MAKEFILE PARTS INCLUDES

.PHONY: provision
provision: config-kube namespace kube-cert ## provisioning the deployment and service
	# change build number for rolling update
	@echo BUILD_NUMBER: ${BUILD_NUMBER}
	cat ${TEMPLATES}/deployment.yml | envsubst | kubectl apply -f -
	cat ${TEMPLATES}/service.yml | envsubst | kubectl replace --force -f -

.PHONY: namespace
namespace: config-kube ## Create name space if it doens't exist.
	@if ! kubectl get namespace ${APP_NAMESPACE} &> /dev/null ; then \
		echo Create ${APP_NAMESPACE}; \
		cat ${TEMPLATES}/namespace.yml | envsubst | kubectl apply -f - ; \
	else \
		echo ${APP_NAMESPACE} already exist. ; \
	fi

.PHONY: destroy-namespace
destroy-namespace: config-kube ## delete namespace
	@if kubectl get namespace ${APP_NAMESPACE} &> /dev/null ; then \
		echo Delete ${APP_NAMESPACE}; \
		cat ${TEMPLATES}/namespace.yml | envsubst | kubectl delete -f - ; \
	else \
		echo ${APP_NAMESPACE} do not exist. ; \
	fi

.PHONY: update
update: config-kube ## rolling upgrade the deployment
	# change build number for rolling update
	@echo BUILD_NUMBER: ${BUILD_NUMBER}
	cat ${TEMPLATES}/deployment.yml | envsubst | kubectl apply -f -

.PHONY: force-update
force-update: ## Force update - delete deployment and re-deploy
	@echo BUILD_NUMBER: ${BUILD_NUMBER}
	cat ${TEMPLATES}/deployment.yml  | envsubst | kubectl delete --ignore-not-found -f -
	cat ${TEMPLATES}/deployment.yml | envsubst | kubectl apply -f -

.PHONY: DESTROY
DESTROY: config-kube destroy-kube-cert ## destroy the deployment and service
	@cat ${TEMPLATES}/deployment.yml  | envsubst | kubectl delete --ignore-not-found -f -
	# Not destroy service if not necessary (e.g elb takes time to build)
	#cat ${TEMPLATES}/service.yml | envsubst | kubectl delete --ignore-not-found  -f -

.PHONY: destroy-svc
destroy-svc: config-kube ## Destroy service. ELB will be removed.
	@cat ${TEMPLATES}/service.yml | envsubst | kubectl delete --ignore-not-found  -f -

.PHONY: update-svc
#update-svc: CERT_ARN=$$(arn:aws:iam::${AWS_ACCOUNT}:server-certificate/${APP_FQDN})
update-svc: config-kube ## Update service
	cat ${TEMPLATES}/service.yml | envsubst
	@cat ${TEMPLATES}/service.yml | envsubst | kubectl apply -f -
	@$(MAKE) update-svc-cert

.PHONY: update-svc-cert
update-svc-cert: ## Update service certificate
	@${SCRIPTS_DIR}/upload-aws-cert.sh

.PHONY: get-all
get-all: ## show all resources in this namespace
	kubectl get all -n ${APP_NAMESPACE}

.PHONY: history
history: ## show deployment history
	kubectl rollout history deployments/${APP} -n ${APP_NAMESPACE}
