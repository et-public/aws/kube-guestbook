# Shared cross environments. Can be override in each environment's env.sh
export AWS_REGION=us-west-2
export AWS_DEFAULT_AZ=${AWS_REGION}a

# Local configurations
export SCRIPTS_REPO=https://code.stanford.edu/et-public/cloud-scripts.git
export SCRIPTS_DIR=${HOME}/bin/cloud-scripts

# Let's encrypt issuer
export LETS_ENCRYPT_ISSUER=letsencrypt-staging

# Applictaion
export APP=guestbook
export APP_SVC=${APP}
export APP_NAMESPACE=${APP}
export KUBE_CERT_NAME=${APP}

# Defaut pvc for dynamic disk allocation
export DATA_DISK_SIZE=200Gi
export STORAGE_CLASS_ZONE=${AWS_DEFAULT_AZ}
export STORAGE_CLASS_TYPE=gp2
export STORAGE_CLASS_NAME=${STORAGE_CLASS_ZONE}-${STORAGE_CLASS_TYPE}
