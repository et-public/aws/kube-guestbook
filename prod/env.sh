export APP_ENVIRONMENT=prod
export CLUSTER_NAME=authnz-kube-prod
export AWS_PROFILE=authnz-prod
export KUBECONFIG=${HOME}/.kube/${CLUSTER_NAME}

export ROUTE53_ZONE_NAME=authnz.stanford.edu
export ZONE_NAME=authnz.stanford.edu. # for jq select.
# aws --profile authnz-prod route53 list-hosted-zones --output json | jq -r '.HostedZones[] | select(.Name=="authnz.stanford.edu.") | .Id '
export ROUTE53_ZONE_ID=`aws --profile ${AWS_PROFILE} route53 list-hosted-zones --output json | jq -r ".HostedZones[] | select(.Name==env.ZONE_NAME) | .Id "`
export ROUTE53_ZONE_ID={ROUTE53_ZONE_ID#/hostedzone/}

# Applictaion
export APP=guestbook
export APP_FQDN=${APP}.${ROUTE53_ZONE_NAME}
export APP_SVC=${APP}
export APP_NAMESPACE=${APP}
