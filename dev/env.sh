# Environment applictaion specifics
export APP_ENVIRONMENT=dev
export CLUSTER_NAME=authnz-kube-x
export AWS_PROFILE=authnz-x
export AAWS_ACCOUNT := $(shell aws --profile ${AWS_PROFILE} sts get-caller-identity | jq -r '.Account' )
export KUBECONFIG=${HOME}/.kube/${CLUSTER_NAME}

export ROUTE53_ZONE_NAME=authnz-x.stanford.edu
export ZONE_NAME=authnz-x.stanford.edu. # for jq select.
# aws --profile authnz-x route53 list-hosted-zones --output json | jq -r '.HostedZones[] | select(.Name=="authnz-x.stanford.edu\.") | .Id '
export ROUTE53_ZONE_ID=`aws --profile ${AWS_PROFILE} route53 list-hosted-zones --output json | jq -r ".HostedZones[] | select(.Name==env.ZONE_NAME) | .Id "`
export ROUTE53_ZONE_ID={ROUTE53_ZONE_ID#/hostedzone/}
export APP_FQDN=${APP}.${ROUTE53_ZONE_NAME}

# Let's encrypt issuer
export LETS_ENCRYPT_ISSUER=letsencrypt-production

# Default disks
export DATA_DISK_SIZE=100Gi